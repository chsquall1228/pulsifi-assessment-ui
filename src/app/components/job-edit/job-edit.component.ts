import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, RequiredValidator, Validators } from '@angular/forms';
import { JobService } from 'src/app/services/job/job.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-job-edit',
  templateUrl: './job-edit.component.html',
  styleUrls: ['./job-edit.component.scss']
})
export class JobEditComponent implements OnInit {

  form: FormGroup;
  file: any;
  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private jobService: JobService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    if(!this.authService.isLogin()){
      this.router.navigate(['/']);
    }
    this.form = new FormGroup({
      id: new FormControl(),
      title: new FormControl('', Validators.required),
      location: new FormControl(),
      description: new FormControl(),
      date: new FormControl(),
      status: new FormControl()
    });

    let id = this.route.snapshot.paramMap.get('id');
    if(id){
      this.jobService.get(id).subscribe((response) => {
        this.form.controls.id.patchValue(response.id);
        this.form.controls.title.patchValue(response.title);
        this.form.controls.description.patchValue(response.description);
        this.form.controls.date.patchValue(response.date);
        this.form.controls.location.patchValue(response.location);
        this.form.controls.status.patchValue(response.status);
      });
    }
  }

  handleSubmit(){
    if(this.form.valid){
      let value = this.form.value;
      let subscriber;
      if(value.id){
        subscriber = this.jobService.put(value.id, value);
      }else{
        subscriber = this.jobService.post(value);
      }
      subscriber.subscribe((response) => {
        if(this.file){
          this.jobService.postFile(response.id, this.file).subscribe((response) => {
            this.snackBar.open("Data has been saved", null, { duration: 2000, panelClass : 'success'});
          }, (response) => {
            let error = JSON.parse(response.error);
            this.snackBar.open(error.message, null, { duration: 2000, panelClass : 'danger'});
          });
        }else{
          this.snackBar.open("Data has been saved", null, { duration: 2000, panelClass : 'success'});
        }
      }, (response) => {
        let error = JSON.parse(response.error);
        this.snackBar.open(error.message, null, { duration: 2000, panelClass : 'danger'});
      })
    }
  }

  onFileSelected(event){
    if(event.target.files.length > 0 && event.target.files[0].type == 'application/pdf' && event.target.files[0].size < 2000000){
      this.file = event.target.files[0];
    }else{
      this.snackBar.open("File doesn't fulfill requirement", null, { duration: 2000, panelClass : 'danger'} )
    }
  }
}
