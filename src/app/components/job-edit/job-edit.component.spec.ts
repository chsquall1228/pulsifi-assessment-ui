import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobEditComponent } from './job-edit.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobService } from 'src/app/services/job/job.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';

describe('JobEditComponent', () => {
  let component: JobEditComponent;
  let fixture: ComponentFixture<JobEditComponent>;
  let jobService, authService, router, getId;

  beforeEach(async(() => {
    jobService = jasmine.createSpyObj('JobService', ['get', 'post', 'put', 'postFile']);
    authService = jasmine.createSpyObj('AuthService', ['isLogin']);
    router = jasmine.createSpyObj('Router', ['navigate']);
    getId = jasmine.createSpy()
    TestBed.configureTestingModule({
      declarations: [ JobEditComponent ],
      providers: [
        { provide: AuthService, useValue: authService},
        { provide: JobService, useValue: jobService},
        { provide: ActivatedRoute, useValue: {snapshot: {paramMap: { get: getId}}}},
        { provide: Router, useValue: router},
        {provide: MatSnackBar, useValue: jasmine.createSpyObj('MatSnackBar',['open'])},
      ]
    })
    .compileComponents();
  }));

  it('should go to main page', () => {
    authService.isLogin.and.returnValue(false);
    fixture = TestBed.createComponent(JobEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalled();
  })

  it('should create', () => {
    authService.isLogin.and.returnValue(true);
    getId.and.returnValue(1);
    jobService.get.and.returnValue(of({title: 'Test', description: 'Test Description', location: "Kuala Lumpur", date: new Date(2020, 1, 1), status: false}))
    fixture = TestBed.createComponent(JobEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.form.value.title).toEqual('Test');
    expect(component.form.value.description).toEqual('Test Description');
    expect(component.form.value.location).toEqual('Kuala Lumpur');
    expect(component.form.value.status).toEqual(false);
  });

  it('should submit post with file', () => {
    authService.isLogin.and.returnValue(true);
    getId.and.returnValue(1);
    jobService.get.and.returnValue(of({title: 'Test', description: 'Test Description', location: "Kuala Lumpur", date: new Date(2020, 1, 1), status: false}))
    jobService.post.and.returnValue(of({id: 1}))
    jobService.postFile.and.returnValue(of())
    fixture = TestBed.createComponent(JobEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.file = {};
    component.handleSubmit();
    expect(jobService.post).toHaveBeenCalled();
    expect(jobService.postFile).toHaveBeenCalled();
  })

  it('should submit put', () => {
    authService.isLogin.and.returnValue(true);
    getId.and.returnValue(1);
    jobService.get.and.returnValue(of({id: 1, title: 'Test', description: 'Test Description', location: "Kuala Lumpur", date: new Date(2020, 1, 1), status: false}))
    jobService.put.and.returnValue(of({id: 1}))
    fixture = TestBed.createComponent(JobEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.handleSubmit();
    expect(jobService.put).toHaveBeenCalled();
  })
});
