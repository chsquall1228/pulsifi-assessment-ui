import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobComponent } from './job.component';
import { JobService } from 'src/app/services/job/job.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { Observable, of } from 'rxjs';

describe('JobComponent', () => {
  let component: JobComponent;
  let jobService;
  let snackBar;
  let fixture: ComponentFixture<JobComponent>;

  beforeEach(async(() => {
    jobService = jasmine.createSpyObj('JobService', ['get']);
    snackBar = jasmine.createSpyObj('MatSnackBar',['open']);
    TestBed.configureTestingModule({
      declarations: [ JobComponent ],
      providers: [
        {provide: JobService, useValue: jobService},
        {provide: HttpClient, useValue: jasmine.createSpyObj('HttpClient',['get'])},
        {provide: MatSnackBar, useValue: snackBar},
        {provide: ActivatedRoute, useValue: {snapshot: {paramMap: { get: jasmine.createSpy()}}}}
      ]
    })
    .compileComponents();
  }));


  it('should create disable', () => {
    jobService.get.and.returnValue(of({title: 'Test', description: 'Test Description', location: "Kuala Lumpur", date: new Date(2020, 1, 1), status: false}))
    fixture = TestBed.createComponent(JobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(component).toBeTruthy();
    expect(snackBar.open).toHaveBeenCalled();
    expect(compiled.querySelector('mat-card-title').textContent).toContain('Test');
    expect(compiled.querySelector('mat-card-subtitle').textContent).toContain('2020');
    expect(compiled.querySelector('mat-card-subtitle.location').textContent).toContain('Kuala Lumpur');
    expect(compiled.querySelector('mat-card-content').textContent).toContain('Test Description');
  });

  
  it('should create disabled', () => {
    jobService.get.and.returnValue(of({title: 'Test', description: 'Test Description', location: "Kuala Lumpur", date: new Date(2020, 1, 1), status: true}))
    fixture = TestBed.createComponent(JobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(component).toBeTruthy();
    expect(compiled.querySelector('mat-card-title').textContent).toContain('Test');
    expect(compiled.querySelector('mat-card-subtitle').textContent).toContain('2020');
    expect(compiled.querySelector('mat-card-subtitle.location').textContent).toContain('Kuala Lumpur');
    expect(compiled.querySelector('mat-card-content').textContent).toContain('Test Description');
  });
});
