import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Job } from 'src/app/models/job';
import { JobService } from 'src/app/services/job/job.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit {
  job: Job = {};
  constructor(
    private route: ActivatedRoute,
    private jobService: JobService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.jobService.get(id).subscribe((response) => {
      this.job = response;
      if(!this.job.status){
        this.snackBar.open("This job has been disabled", null, { duration: 2000});
      }
    })
  }

}
