import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  constructor(
    private router: Router,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  handleSubmit(){
    if(this.form.valid){
      this.authService.login(this.form.value.username, this.form.value.password).subscribe(() => {
        this.snackBar.open("Login success", null, { duration: 2000, panelClass : 'success'});
        this.router.navigate(['jobs']);
      }, (response) => {
        let error = JSON.parse(response.error)
        this.snackBar.open(`${error.message}`, null, { duration: 2000, panelClass : 'danger'});
      });
    }
  }
}
