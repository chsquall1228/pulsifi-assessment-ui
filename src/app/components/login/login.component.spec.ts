import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let snackBar, authService, router;

  beforeEach(async(() => {
    snackBar = jasmine.createSpyObj('MatSnackBar', ['open']);
    authService = jasmine.createSpyObj('AuthService', ['login']);
    router = jasmine.createSpyObj('Router', ['navigate'])
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [
        { provide: Router, useValue: router},
        { provide: AuthService, useValue: authService},
        { provide: MatSnackBar, useValue: snackBar}
      ]
    })
    .compileComponents();
  }));


  it('should create', () => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should login success', () => {
    authService.login.and.returnValue(of({}));
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.form.controls.username.patchValue('admin');
    component.form.controls.password.patchValue('admin');
    component.handleSubmit();
    expect(authService.login).toHaveBeenCalled();
    expect(snackBar.open).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalled();
  })
});
