import { Component, OnInit } from '@angular/core';
import { Job } from 'src/app/models/job';
import { JobService } from 'src/app/services/job/job.service';
import { Pagination } from 'src/app/models/pagination';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  dataSource:Job[]; 
  offset: number = 0;
  size:number;
  limit:number = 50;
  pageSizeOptions: number[] = [5, 10, 25, 50];
  displayedColumns: string[] = ['title', 'status'];
  constructor(
    private authService: AuthService,
    private jobService: JobService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    if(this.authService.isLogin()){
      this.displayedColumns.push('action');
    }
    this.handleData();
  }

  handleData(){
    this.jobService.index(this.offset, this.limit).subscribe((response: Pagination<Job>) => {
      this.dataSource = response.items;
      this.size = response.size;
    });
  }

  handleEdit(item){
    this.router.navigate(['jobs', item.id, 'edit'])
  }

  handleDelete(item){
    this.jobService.delete(item.id).subscribe((response) => {
      this.handleData();
      this.snackBar.open('Data has deleted', null, {duration: 2000, panelClass: 'success'});
    },(response) => {
      this.snackBar.open('Data fail to delete', null, {duration: 2000, panelClass: 'danger'});
    })
  }

  pageChanges($event: PageEvent){
    this.limit = $event.pageSize;
    this.offset = $event.pageIndex*this.limit;
    this.handleData();
  }

  getRecord(item: Job){
    this.router.navigate(['jobs', item.id]);
  }
}
