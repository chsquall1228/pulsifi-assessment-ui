import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsComponent } from './jobs.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { JobService } from 'src/app/services/job/job.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

describe('JobsComponent', () => {
  let component: JobsComponent;
  let fixture: ComponentFixture<JobsComponent>;
  let jobService, authService, snackBar, router;

  beforeEach(async(() => {
    jobService = jasmine.createSpyObj('JobService', ['index', 'delete']);
    snackBar = jasmine.createSpyObj('MatSnackBar', ['open']);
    authService = jasmine.createSpyObj('AuthService', ['isLogin']);
    router = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      declarations: [ JobsComponent ],
      providers: [
        { provide: AuthService, useValue: authService},
        { provide: Router, useValue: router},
        { provide: JobService, useValue: jobService},
        { provide: MatSnackBar, useValue: snackBar},
      ]
    })
    .compileComponents();
    jobService.index.and.returnValue(of({}));
  }));

  it('should create', () => {
    authService.isLogin.and.returnValue(true);
    fixture = TestBed.createComponent(JobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.displayedColumns).toContain('action');
    expect(component).toBeTruthy();
    expect(jobService.index).toHaveBeenCalled();
  });

  it('should delete record', () => {
    authService.isLogin.and.returnValue(true);
    jobService.delete.and.returnValue(of({}));
    fixture = TestBed.createComponent(JobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.handleDelete({id: 1});
    expect(jobService.delete).toHaveBeenCalledWith(1);
    expect(snackBar.open).toHaveBeenCalled();
  })

  it('should navigate to single job', () => {
    authService.isLogin.and.returnValue(true);
    fixture = TestBed.createComponent(JobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.getRecord({id: '1'});
    expect(router.navigate).toHaveBeenCalled();
  })

  it('should update offset', () => {
    authService.isLogin.and.returnValue(true);
    fixture = TestBed.createComponent(JobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.pageChanges({pageIndex: 1, pageSize: 3} as PageEvent);
    expect(component.offset).toEqual(50);
  })
});
