import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from "@angular/core";
import { LayoutComponent } from './components/layout/layout.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { JobComponent } from './components/job/job.component';
import { LogoutComponent } from './components/logout/logout.component';
import { JobEditComponent } from './components/job-edit/job-edit.component';

const appRoutes: Routes = [
    {path: '', redirectTo: 'jobs', pathMatch: 'full'},
    {path: '', component: LayoutComponent, children: [
        {path: 'login', component: LoginComponent},
        {path: 'logout', component: LogoutComponent},
        {path: 'jobs', component: JobsComponent},
        {path: 'jobs/add', component: JobEditComponent},
        {path: 'jobs/:id', component: JobComponent},
        {path: 'jobs/:id/edit', component: JobEditComponent}
    ]}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports:[RouterModule]
})

export class AppRoutingModule{}