import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import { catchError, concat } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LocalStorage } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class HttpHeadersInterceptor implements HttpInterceptor {

  constructor(protected router : Router){
  }

  @LocalStorage("token") token: string;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = {
    };
    if(!(req.body instanceof FormData)){
      headers['Content-Type'] = 'application/json';
    }
    if(this.token){
      headers['Authorization'] = `Bearer ${this.token}`;
    }
    const request = req.clone({
      setHeaders: headers
    });
    return next.handle(request).pipe(catchError(error => {
      if(error.status === 401){
        this.router.navigate(['/login']);
      }
      throw error;
    }));
  };
}
