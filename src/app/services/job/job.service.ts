import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseHttpClientService } from '../base-http-client.service';
import { Job } from 'src/app/models/job';

@Injectable({
  providedIn: 'root'
})
export class JobService extends BaseHttpClientService<Job>{
  constructor(protected http: HttpClient) { 
    super(http, 'api/v1/jobs');
  }
  
  postFile(id, file){
    let formData = new FormData();
    formData.append('file', file);
    return this.http.post<any>(this.__url(`${this.section}/${id}/file`), formData);
  }
}
