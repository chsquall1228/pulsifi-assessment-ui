import { TestBed, inject } from '@angular/core/testing';

import { JobService } from './job.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

describe('JobService', () => {
  let httpClient;
  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient',['get', 'post'])
    TestBed.configureTestingModule({
      providers: [JobService, 
        { provide: HttpClient, useValue: httpClient }
      ]
    });
  });

  it('should be created', inject([JobService], (service: JobService) => {
    expect(service).toBeTruthy();
  }));

  it('should be post file', inject([JobService], (service: JobService) => {
    expect(service).toBeTruthy();
    httpClient.post.and.returnValue(of({}));
    service.postFile(1, {name: 'file'});
    expect(httpClient.post).toHaveBeenCalled();
  }));
});
