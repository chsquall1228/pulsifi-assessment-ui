import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocalStorageService, NgxWebstorageModule } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';

describe('AuthService', () => {
  let httpClient, localStorage;
  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient',['get', 'post', 'put', 'delete']);
    localStorage = jasmine.createSpyObj('LocalStorageService', ['retrieve', 'store', 'clear']);
    TestBed.configureTestingModule({
      providers: [AuthService,
        { provide: HttpClient, useValue:  httpClient},
        { provide: LocalStorageService, useValue: localStorage}
      ]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));


  it('should be login', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
    localStorage.retrieve.and.returnValue("tokens");
    httpClient.post.and.returnValue(of({access_token: 'token', user: 'username'}));
    service.login('admin', 'admin').subscribe();
    expect(httpClient.post).toHaveBeenCalled();
    expect(localStorage.store).toHaveBeenCalled();
    expect(service.navItems).toContain({path: '/logout', label: 'Logout'});
  }));

  it('should be logged in', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
    localStorage.retrieve.and.returnValue("tokens");
    expect(service.isLogin()).toBeTruthy();
    expect(localStorage.retrieve).toHaveBeenCalled();
  }));

  
  it('should be logged out', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
    service.logout();
    expect(localStorage.clear).toHaveBeenCalled();
    expect(service.navItems).toContain({path: '/login', label: 'Login'})
  }));
});
