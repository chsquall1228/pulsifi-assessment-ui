import { Injectable } from '@angular/core';
import { BaseHttpClientService } from '../base-http-client.service';
import {  LocalStorageService } from 'ngx-webstorage';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseHttpClientService<any>{
  navItems: any[];

  constructor(protected http: HttpClient, private localStorage: LocalStorageService) { 
    super(http, 'api/v1/auth');
    this.updatePath();
  }

  public login(username, password){
    let observable = new Observable((observer) =>{
      this.http.post<any>(this.__url(`${this.section}/login`), {
        username,
        password,
      }).subscribe(({access_token, user}) => {
        this.localStorage.store('token', access_token);
        this.updatePath();
        observer.next({access_token, user});
      }, (error) =>{
        observer.error(error);
      });
    });
    return observable;
  }

  public isLogin(){
    return this.localStorage.retrieve('token');
  }
  
  public logout(){
    this.localStorage.clear();
    this.updatePath();
  }

  private updatePath(){
    this.navItems = [
      {path: '/jobs', label: 'Jobs'},
    ];

    if(this.isLogin()){
      this.navItems.push({path: '/logout', label: 'Logout'});
    }else{
      this.navItems.push({path: '/login', label: 'Login'});
    }
  }
}
