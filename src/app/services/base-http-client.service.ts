import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {environment} from '../../environments/environment';
import { forkJoin } from "rxjs";
import { Pagination } from '../models/pagination';

export class BaseHttpClientService<T> {

  protocol: string;
  hostname: string;
  port: number;
  section: string;

  constructor(protected http: HttpClient, section ?: string) {
    this.protocol = location.protocol;
    this.hostname = location.hostname;
    this.port = 8888;
    this.section = section;
  }

  static headers() {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return headers;
  }
  
  public index(offset?: number, limit?:number, sorts?: string[], queries?: any) {
    let params = new HttpParams();
    params = params.set('offset', (offset || 0)+'');
    params = params.set('limit', (limit || 25)+'');
    if(sorts){
      params = params.set('sorts', sorts.join(','));
    }
    if(queries){
      for(let query in queries){
        if(queries[query] && queries[query].length > 0){
          params = params.set(query, queries[query]);
        }
      }
    }
    return this.http.get<Pagination<T>>(this.__url(this.section), {params});
  }

  public deletes(ids){
    let limit = 20;
    let tempIds = [];
    let tempId = [];
    for(let i = 0; i < ids.length; i++){
      tempId.push(ids[i]);
      if(tempId.length == limit)
      {
        tempIds.push(tempId);
        tempId = [];
      }
    }
    if(tempId.length > 0)
    {
      tempIds.push(tempId);
    }
    
    return forkJoin(tempIds.map((id) =>{
      let params = new HttpParams();
      params = params.set('ids', id.join(','));
      return this.http.delete(this.__url(`${this.section}`), {params});
    }));
  }

  public delete(id){
    return this.http.delete(this.__url(`${this.section}/${id}`));
  }

  public get(id) {
    return this.http.get<T>(this.__url(`${this.section}/${id}`));
  }

  public post(body:T)
  {
    return this.http.post(this.__url(`${this.section}`), body);
  }

  public put(id, body:T)
  {
    return this.http.put(this.__url(`${this.section}/${id}`), body);
  }

  protected __url(section) {
    return `${environment.apiUrl}/${section}`;
  }
}
