export interface Job {
    id ?: string;
    title ?: string;
    location ?: string;
    description ?: string;
    date ?: Date;
    status ?: boolean;
    file ?: string;
}
  