export interface Pagination<T> {
    size: number;
    items: T[];
  }
  