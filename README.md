# PulsifiAssessmentUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## Development server
Run `npm run start` to start a development server

## Unit Test
Run `npm run test` to start unit testing checking

## Generate Production Code
Run `npm run build` to build production code
